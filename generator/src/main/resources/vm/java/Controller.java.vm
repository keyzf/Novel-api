package ${package}.controller;

import com.novel.framework.annotation.Log;
import com.novel.framework.enums.BusinessType;
import com.novel.framework.result.Result;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ${package}.domain.${className};
import ${package}.service.${className}Service;
import com.novel.framework.base.BaseController;
import com.novel.framework.web.page.TableDataInfo;
import com.novel.framework.validate.groups.AddGroup;
import com.novel.framework.validate.groups.EditGroup;

/**
 * ${tableComment} 信息操作处理
 * 
 * @author ${author}
 * @date ${datetime}
 */
@RestController
@RequestMapping("/${moduleName}/${classname}")
public class ${className}Controller extends BaseController{

	private final ${className}Service ${classname}Service;

	public ${className}Controller(${className}Service ${classname}Service) {
		this.${classname}Service = ${classname}Service;
	}

	/**
	 * 查询${tableComment}列表
	 */
	@RequiresPermissions("${moduleName}:${classname}:list")
	@GetMapping("/list")
	public TableDataInfo list(${className} ${classname}){
		startPage();
		return getDataTable(${classname}Service.select${className}List(${classname}));
	}



	/**
    * 新增${tableComment}
    *
    * @param ${classname} ${tableComment}
    * @return 操作结果
    */
	@Log(title = "${tableComment}", businessType = BusinessType.INSERT)
	@RequiresPermissions("${moduleName}:${classname}:add")
	@PostMapping("/add")
	public Result addSave(@Validated(AddGroup.class) ${className} ${classname}) {
		return toAjax(${classname}Service.insert${className}(${classname}), "新增${tableComment}成功", "新增${tableComment}失败");
	}

	/**
	* 保存编辑${tableComment}
	*
	* @param ${classname} ${tableComment}
	* @return 操作结果
	*/
	@Log(title = "${tableComment}", businessType = BusinessType.UPDATE)
	@RequiresPermissions("${moduleName}:${classname}:edit")
	@PutMapping("/edit")
	public Result editSave(@Validated(EditGroup.class) ${className} ${classname}) {
		return toAjax(${classname}Service.update${className}(${classname}), "修改${tableComment}成功！", "修改${tableComment}失败");
	}

	/**
    * 删除${tableComment}
    *
    * @param ${primaryKey.attrname}s ${tableComment}ID
    * @return 操作结果
    */
	@Log(title = "${tableComment}", businessType = BusinessType.DELETE)
	@RequiresPermissions("${moduleName}:${classname}:remove")
	@DeleteMapping("/remove")
	public Result remove(${primaryKey.attrType}[] ${primaryKey.attrname}s) {
		return toAjax(${classname}Service.batchDelete${className}(${primaryKey.attrname}s), "删除${tableComment}成功", "删除${tableComment}失败");
	}
}
