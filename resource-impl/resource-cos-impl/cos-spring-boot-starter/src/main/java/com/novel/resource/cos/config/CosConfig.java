package com.novel.resource.cos.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * cos 配置
 *
 * @author novel
 * @date 2020/1/2
 */
@Data
@ConfigurationProperties(prefix = CosConfig.COS_PREFIX)
public class CosConfig {
    public static final String COS_PREFIX = "cos";
    /**
     * 桶的名称
     */
    private String bucketName;
    /**
     * 区域
     */
    private String region;
    /**
     * APP ID
     */
    private String appId;
    /**
     * SecretId 是用于标识 API 调用者的身份
     */
    private String secretId;
    /**
     * SecretKey是用于加密签名字符串和服务器端验证签名字符串的密钥
     */
    private String secretKey;
}
