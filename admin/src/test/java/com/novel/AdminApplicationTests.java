package com.novel;

import com.novel.domain.TableInfo;
import com.novel.mapper.GenMapper;
import lombok.extern.slf4j.Slf4j;
import org.jasypt.encryption.StringEncryptor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class AdminApplicationTests {
    // 注入StringEncryptor
    @Autowired
    private StringEncryptor stringEncryptor;
    @Autowired
    private GenMapper genMapper;

    @Test
    public void contextLoads() {
        // 加密数据库用户名
        String username = stringEncryptor.encrypt("root");
        System.out.println(username);
    }

    @Test
    public void contextLoads2() {
        List<TableInfo> tableInfos = genMapper.selectTableList(new TableInfo());
        System.out.println(tableInfos);
    }

}
